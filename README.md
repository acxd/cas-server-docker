CAS Server Build
===================

This repository consists of the CAS Server that is to be built and deployed to an external server.
The base of the CAS Server is the [CAS WAR overlay] (https://github.com/apereo/cas-overlay-template), commit 0bd0c62 was used, modifications have been made to the configuration to ensure it works as a standalone application, to authenticate other ACX applications.

# Versions

```xml
<cas.version>5.2.0</cas.version>
```

# Requirements

* JDK 1.8+

# Configuration

The `etc` directory contains the configuration files and directories that will be copied to `/etc/cas/config` upon build. 

The application.properties that resides in src/main/resources, is utilised for storing the database configuration details.

# Set up instructions

Executing the following script, will build the application.

```bash
./build.sh
```

To package the final web application, and run the CAS Server locally 

```bash
./build.sh package
```

To package the final web application, and run the CAS Server at an external container (like Tomcat) 

```bash
./build.sh run 
```
Deploy resultant `target/cas.war`  to a servlet container of choice.

# Deployment

Deploy resultant `target/cas.war`  to a servlet container of choice.

Create a keystore file `thekeystore` under `/etc/cas`. 

Edit the servlet configuration settings to accept connections via 8443.

On a successful deployment via the following methods, CAS will be available at:

* `http://cas.server.name:8080/cas`
* `https://cas.server.name:8443/cas`


#Repo Owner

The repo owner is Fiona Man, contact Adarsh if you have any queries on deploying this application.
